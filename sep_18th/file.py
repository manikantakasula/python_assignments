import pandas as pd

def total_percent(filename):

	data = pd.read_csv(filename)
	data["Total"] = data[['math', 'science','arts']].sum(axis=1) 
	data["Percentage"]= data[["Total"]].div(3).round(1)
	print(data)

	data.to_csv(filename)

filename ="mani.csv"

total_percent(filename)
