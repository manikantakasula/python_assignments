import pytest

from palindrome import ispalindrome

def test_palindrome():
  assert ispalindrome(["malayalam"]==True, "testing completed")
  assert ispalindrome(["test"]==False, "testing completed")