def ispalindrome(dt):
    if type(dt) != str and type(dt) != list:
        return False
    if type(dt)==str:
        dt=dt.lower()
    reverse=dt[::-1]
    if dt == reverse:
        return True
    return False

if __name__=='__main__':
    print(ispalindrome("maninam"))
    print(ispalindrome([3,4,5,4,3]))
    print(ispalindrome("manikanta"))
