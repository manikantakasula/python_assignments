import pytest
from linkedlist import linkedlist_operations

list_empty_error = "LinkedList is empty, doesn't have elements to remove"
empty="list is empty"

def test_linkedlist():
    l1=linkedlist_operations()
    assert(list_empty_error==l1.removefirstelement())

    l1.prepend(10)
    l1.prepend("hello")
    l1.prepend(None)
    l1.prepend(1.2)
    assert([1.2,None,"hello",10]==l1.printlist())

    l1.removefirstelement()
    l1.removefirstelement()
    assert(["hello",10]==l1.printlist())

    l1.prepend({1,2,3,4})
    assert([{1,2,3,4},"hello",10]==l1.printlist())
    
    l1.prepend([1,2,{"username":"Nan"},"Test"])
    assert([[1,2,{"username":"Nan"},"Test"],{1,2,3,4},'hello',10]==l1.printlist())

    l1.removefirstelement()
    assert([{1,2,3,4},'hello',10]==l1.printlist())

    l1.removefirstelement()
    assert(['hello',10]==l1.printlist())

    l1.removefirstelement()
    l1.removefirstelement()
    assert(list_empty_error==l1.removefirstelement())
