import pytest
from calculator import add,divide
def test_add():
    result=add(3,4)
    assert result==7
    assert add(8,5)==13

def test_divide():
    assert divide(4,2)==2
    assert divide(10,4)==2.5
