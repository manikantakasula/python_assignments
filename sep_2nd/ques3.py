openbraces = ["[","{","("]
closingbraces = ["]","}",")"]

def isbalanced(input):
	stack = []
	for i in input:
		if i in openbraces:
			stack.append(i)
		elif i in closingbraces:
			pos = closingbraces.index(i)
			if ((len(stack) > 0) and
				(openbraces[pos] == stack[len(stack)-1])):
				stack.pop()
			else:
				return "Unbalanced"
	if len(stack) == 0:
		return "Balanced"
	else:
		return "Unbalanced"


filename=input()

text_file = open(filename, "r")
data = text_file.read()
text_file.close()

input = str(data)
print(input,"-", isbalanced(input))
