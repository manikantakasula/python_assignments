class Node:

	def __init__(self, data):
		self.data = data
		self.next = None

class Stack:

	def __init__(self):
		self.head = None

	def isempty(self):
		if self.head == None:
			return True
		else:
			return False

	def push(self, data):
		if self.head == None:
			self.head = Node(data)
		else:
			newnode = Node(data)
			newnode.next = self.head
			self.head = newnode

	def pop(self):
		if self.isempty():
			return None
		else:
			poppednode = self.head
			self.head = self.head.next
			poppednode.next = None
			return poppednode.data

	def display(self):
		if self.isempty():
			print("Stack Underflow")
		else:
			iternode = self.head
			n=''
			while(iternode != None):
				n+=iternode.data
				iternode = iternode.next
			return n
 
def isbalanced(str1):  
	count= 0  
	ans=False  
	for i in str1:  
		if i == "(" or i == "{" or i == "[":  
			count += 1  
		elif i == ")" or i == "}" or i == "]":  
			count-= 1  
		if count < 0:  
			return ans  
	if count==0:  
		return not ans  
	return ans  

if __name__ == "__main__":
    MyStack = Stack()
    
    MyStack.push(']')  
    MyStack.push('}')
    MyStack.push(')')
    MyStack.push('(')
    MyStack.push(')')
    MyStack.push('(')
    MyStack.push('{')
    
    
    stack_data = MyStack.display()
    
    input = stack_data
    print(input,'-',isbalanced(input))
